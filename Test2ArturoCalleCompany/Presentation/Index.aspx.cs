﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Index : System.Web.UI.Page
    {
        Company objCompany = new Company();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_calcular_Click(object sender, EventArgs e)
        {
            double  result;
            string msj_discount;

            objCompany.Price = Convert.ToDouble(TBprice.Text);
            objCompany.Number_garments = Convert.ToInt32(TBnumber.Text);
            result = objCompany.showPrice();
            msj_discount = objCompany.showDiscount();
            Lblmsj1.Text = result.ToString();
            Lblmsj2.Text = msj_discount.ToString();
        }
    }
}