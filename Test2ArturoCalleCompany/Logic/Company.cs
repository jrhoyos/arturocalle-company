﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logic
{
    public class Company
    {
        private int number_garments;
        private double price, discount,subtotal,total_price;
        private string msj_discout;

        public string Msj_discout { get => msj_discout; set => msj_discout = value; }
   
        public double Price { get => price; set => price = value; }
        public double Discount { get => discount; set => discount = value; }
        public int Number_garments { get => number_garments; set => number_garments = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }

        public double showPrice() {

            if (price > 125000)
            {
                discount = 0.35;
                subtotal = price - (price * discount);
                total_price = subtotal * number_garments;
            }
            else if (price <= 125000)
            {
                discount = 0.10;
                subtotal = price -(price * discount);
                total_price = (subtotal * number_garments);
            }
                return total_price ;

        }
        public string showDiscount()
        {
            string aux;
            if (price > 125000)
            {
                aux = " Tiene un descuento del 35% ";
            }
            else if (price <= 125000 && price > 1)
            {
                aux = " Tiene un descuento del 10 %";
            }
            else
            {
                aux = " No tiene descuento ";
            }
            return aux;
        }
    }
}